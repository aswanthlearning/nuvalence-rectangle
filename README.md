# Nuvalence Shapes Feature Analyzer API plan

### Prerequisties
 - Java 11 JDK
 - Maven
 - Docker (Optional)

### Action to do.
```
    - set jdk version to 11 
    - mvn clean install -T1C 
    - After compilation
    - docker build --tag=nuvalence-app:latest .
    - docker run -p8080:8080 nuvalence-app:latest
    
    Test: 
    "POST" http://localhost:8080/api/v1/shape/analyze/Intersection
```
 
### Project Structure/Design
##### Development
 - Micronaut framework (Never tried before thought of learning something new in this interview process)
    ```
     - Leverage on Dependency injection 
     - Jax-RS
     - Embedded netty Server
     - Mock server for testing Endpoints.
    ```
 - Multi-Maven Module.
 - Used JsonSchema2Pojo library to leverage on creating models.
   ```
    - Create models on compile time
    - Reduces the duplication of repeating getters/setters and builders
   ```
 - Low Level Modules doesn't know anything about high level mods.
 - Nuvalence-core module has api's, which only will be exposed to high level clients.
   ```
    - Feature Analyzer Resourses - feature Parity for Resources
    - Boundary Extractor to extract the boundary points for any shape. Implementation is only for Rectangle.
    - Feature Analyzer Api to analyze the features in the shape 
   ```
 - Feature Parity on API resources.
 - Simple Dockerized the app.
 ##### Testing
 - Tried to cover maximum functionalities, 
    ```
        - Used Parameterized tests.
        - Dependency Injection with Micronaut tests. 
    ```
   
### Requirements
#### Common Requirements
 -  Build a shape from points - particularily Rectangle
 -  Analyze features in the shape.
    - Intersection
      ```
        When two rectangles have one or more intersecting lines and produce a result identifying the points of intersection.
      ```
    - Containment
      ```
        When a rectangle is wholly contained within another rectangle.
      ```
    - Adjacent
        ```
          Adjacency is defined as the sharing of at least one side. Side sharing may be proper, sub-line or partial.
        ```
    
#### User story 1 Show the Feature Analyzer Response as Intersection and give intersection points.

- user creates two rectangles with one side of the first rectangle intersects with the side of second rectangle.
- system analyzes two rectangles and sends the result in custom response format. 
- this response will have to be produced with as follows
   ```
   featureType: Intersection.
   feature-subtype: Intersection.
   feature-points: Json array of intersection points
   ```

#### User story 2 Show the Feature Analyzer Response as No Intersection and empty intersection points.

- user creates two rectangles with no side of the first rectangle intersects with the side of second rectangle.
- system analyzes two rectangles and sends the result in custom response format.
- this response will have to be produced with as follows
   ```
   featureType: Intersection.
   feature-subtype: No-Intersection.
   feature-points: emptyList
   ```

#### User story 3 Show the Feature Analyzer Response as Containment and give contained rectangle points.

- user creates two rectangles with first rectangle inside the second rectangle.
- system analyzes two rectangles and sends the result in custom response format.
- this response will have to be produced with as follows
   ```
   featureType: Containment.
   feature-subtype: Containment.
   feature-points: Json array of contained rectangle points
   ```

#### User story 4 Show the Feature Analyzer Response as No Containment and empty intersection points.

- user creates two rectangles with one rectangle is seperated from the second rectangle.
- system analyzes two rectangles and sends the result in custom response format.
- this response will have to be produced with as follows
   ```
   featureType: Containment.
   feature-subtype: No-Containment.
   feature-points: emptyList
   ```

#### User story 5 Show the Feature Analyzer Response as Intersection-No Containment and give intersection points.

- user creates two rectangles with one side of the first rectangle intersects with the side of second rectangle.
- system analyzes two rectangles and sends the result in custom response format.
- this response will have to be produced with as follows
   ```
   featureType: Containment.
   feature-subtype: Intersection-No Containment.
   feature-points: Json array of intersection points
   ```

#### User story 6 Show the Feature Analyzer Response as Adjacent-Proper and give adjacent points.

- user creates two rectangles with one side of the first rectangle adjacent with the side of second rectangle.
- system analyzes two rectangles and sends the result in custom response format.
- this response will have to be produced with as follows
   ```
   featureType: Adjacent.
   feature-subtype: Adjacent-Proper.
   feature-points: Json array of adjacent points
   ```

#### User story 7 Show the Feature Analyzer Response as Adjacent-Partial and give adjacent points.

- user creates two rectangles where some line segment on a side of rectangle A exists as a set of points on some side of Rectangle
- system analyzes two rectangles and sends the result in custom response format.
- this response will have to be produced with as follows
   ```
   featureType: Adjacent.
   feature-subtype: Adjacent-partial.
   feature-points: Json array of adjacent points
   ```

#### User story 8 Show the Feature Analyzer Response as Adjacent-Subline and give adjacent points.

- user creates two rectangles where one side of rectangle A is a line that exists as a set of points wholly contained on some other side of rectangle B,
- system analyzes two rectangles and sends the result in custom response format.
- this response will have to be produced with as follows
   ```
   featureType: Adjacent.
   feature-subtype: Adjacent-Subline.
   feature-points: Json array of adjacent points
   ```

#### User story 9 Show the Feature Analyzer Response as No Adjacent and give empty points.

- user creates two rectangles with one rectangle is seperated from the second rectangle.
- system analyzes two rectangles and sends the result in custom response format.
- this response will have to be produced with as follows
   ```
   featureType: Adjacent.
   feature-subtype: Adjacent-Subline.
   feature-points: Json array of adjacent points
   ```
  
## API Plan

#### Endpoints
```
    /api/v1/shape/analyze/{feature}
        - Intersection
        - Containment
        - Adjacent
```

#### Accept Headers
```
    - application/json
```

#### Request body
```
    - List of Rectangle
```

### Examples
#### Examples 1: Intersection
Request:
```
"POST" http://localhost:8080/api/v1/shape/analyze/Intersection
```
With body
```
[
    {
        "initialPoint": {
            "pointX": 0,
            "pointY": 2
        },
        "width": 10,
        "height": 8
    },
    {
        "initialPoint": {
            "pointX": 3,
            "pointY": 0
        },
        "width": 5,
        "height": 5
    }
]
```

Response as application/json
```
{
    "feature": "Intersection",
    "featureSubType": "Intersection",
    "featurePoints": [
        {
            "pointX": 3,
            "pointY": 2
        },
        {
            "pointX": 8,
            "pointY": 5
        },
        {
            "pointX": 3,
            "pointY": 5
        },
        {
            "pointX": 8,
            "pointY": 2
        }
    ]
}
```
#### Examples 2: No-Intersection
Request:
```
"POST" http://localhost:8080/api/v1/shape/analyze/Intersection
```
With body
```
[
    {
        "initialPoint": {
            "pointX": 0,
            "pointY": 0
        },
        "width": 2,
        "height": 2
    },
    {
        "initialPoint": {
            "pointX": 2,
            "pointY": 3
        },
        "width": 10,
        "height": 10
    }
]
```

Response as application/json
```
{
    "feature": "Intersection",
    "featureSubType": "No Intersection",
    "featurePoints": []
}
```

#### Examples 3: Containment
Request:
```
"POST" http://localhost:8080/api/v1/shape/analyze/Containment
```
With body
```
[
    {
        "initialPoint": {
            "pointX": 0,
            "pointY": 0
        },
        "width": 10,
        "height": 10
    },
    {
        "initialPoint": {
            "pointX": 2,
            "pointY": 2
        },
        "width": 3,
        "height": 3
    }
]
```

Response as application/json
```
{
    "feature": "Containment",
    "featureSubType": "Containment",
    "featurePoints": [
        {
            "pointX": 2,
            "pointY": 2
        },
        {
            "pointX": 5,
            "pointY": 2
        },
        {
            "pointX": 2,
            "pointY": 5
        },
        {
            "pointX": 5,
            "pointY": 5
        }
    ]
}
```

#### Examples 4: No-Containment
Request:
```
"POST" http://localhost:8080/api/v1/shape/analyze/Containment
```
With body
```
[
    {
        "initialPoint": {
            "pointX": 0,
            "pointY": 0
        },
        "width": 10,
        "height": 10
    },
    {
        "initialPoint": {
            "pointX": 11,
            "pointY": 2
        },
        "width": 3,
        "height": 3
    }
]
```

Response as application/json
```
{
    "feature": "Containment",
    "featureSubType": "No Containment",
    "featurePoints": []
}
```

#### Examples 5: Intersection - No-Containment
Request:
```
"POST" http://localhost:8080/api/v1/shape/analyze/Containment
```
With body
```
[
    {
        "initialPoint": {
            "pointX": 0,
            "pointY": 2
        },
        "width": 10,
        "height": 8
    },
    {
        "initialPoint": {
            "pointX": 3,
            "pointY": 0
        },
        "width": 5,
        "height": 5
    }
]
```

Response as application/json
```
{
    "feature": "Containment",
    "featureSubType": "Intersection - No Containment",
    "featurePoints": [
        {
            "pointX": 3,
            "pointY": 2
        },
        {
            "pointX": 8,
            "pointY": 5
        },
        {
            "pointX": 3,
            "pointY": 5
        },
        {
            "pointX": 8,
            "pointY": 2
        }
    ]
}
```

#### Examples 6: Adjacent - Proper
Request:
```
"POST" http://localhost:8080/api/v1/shape/analyze/Adjacent
```
With body
```
[
    {
        "initialPoint": {
            "pointX": 0,
            "pointY": 0
        },
        "width": 10,
        "height": 10
    },
    {
        "initialPoint": {
            "pointX": 10,
            "pointY": 0
        },
        "width": 5,
        "height": 10
    }
]
```

Response as application/json
```
{
    "feature": "Adjacent",
    "featureSubType": "Adjacent (Proper)",
    "featurePoints": [
        {
            "pointX": 10,
            "pointY": 0
        },
        {
            "pointX": 10,
            "pointY": 10
        }
    ]
}
```

#### Examples 7: Adjacent - Partial
Request:
```
"POST" http://localhost:8080/api/v1/shape/analyze/Adjacent
```
With body
```
[
    {
        "initialPoint": {
            "pointX": 0,
            "pointY": 0
        },
        "width": 10,
        "height": 10
    },
    {
        "initialPoint": {
            "pointX": 10,
            "pointY": 3
        },
        "width": 5,
        "height": 10
    }
]
```

Response as application/json
```
{
    "feature": "Adjacent",
    "featureSubType": "Adjacent (Partial)",
    "featurePoints": [
        {
            "pointX": 10,
            "pointY": 3
        },
        {
            "pointX": 10,
            "pointY": 13
        }
    ]
}
```
#### Examples 8: Adjacent - Subline
Request:
```
"POST" http://localhost:8080/api/v1/shape/analyze/Adjacent
```
With body
```
[
    {
        "initialPoint": {
            "pointX": 0,
            "pointY": 0
        },
        "width": 10,
        "height": 10
    },
    {
        "initialPoint": {
            "pointX": 10,
            "pointY": 3
        },
        "width": 5,
        "height": 3
    }
]
```

Response as application/json
```
{
    "feature": "Adjacent",
    "featureSubType": "Adjacent (Sub-line)",
    "featurePoints": [
        {
            "pointX": 10,
            "pointY": 3
        },
        {
            "pointX": 10,
            "pointY": 6
        }
    ]
}
```

#### Examples 8: No Adjacent
Request:
```
"POST" http://localhost:8080/api/v1/shape/analyze/Adjacent
```
With body
```
[
    {
        "initialPoint": {
            "pointX": 0,
            "pointY": 0
        },
        "width": 10,
        "height": 10
    },
    {
        "initialPoint": {
            "pointX": 11,
            "pointY": 0
        },
        "width": 5,
        "height": 10
    }
]
```

Response as application/json
```
{
    "feature": "Adjacent",
    "featureSubType": "Not Adjacent",
    "featurePoints": []
}
```