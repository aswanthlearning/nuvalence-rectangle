package com.interview.nuvalence.api.service;

import com.interview.nuvalence.api.exception.InvalidShapeException;
import com.interview.nuvalence.model.Response;

@FunctionalInterface
public interface FeatureAnalyzer<T> {
    Response analyze(T shape1, T shape2) throws InvalidShapeException;
}
