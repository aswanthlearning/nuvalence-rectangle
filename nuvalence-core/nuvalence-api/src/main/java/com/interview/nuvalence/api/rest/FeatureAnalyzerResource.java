package com.interview.nuvalence.api.rest;

import com.interview.nuvalence.model.Response;
import io.micronaut.http.annotation.Body;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

public interface FeatureAnalyzerResource<T> {

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The feature is analyzed successfully"),
            @ApiResponse(responseCode = "500", description = "The server cannot satisfy the request for any reason")
    })
    @Path("/shape/analyze/{feature}")
    Response getResults(@PathParam("feature") String featureType, @Body List<T> shapes);
}
