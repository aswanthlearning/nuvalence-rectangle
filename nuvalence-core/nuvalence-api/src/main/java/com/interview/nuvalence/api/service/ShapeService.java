package com.interview.nuvalence.api.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.interview.nuvalence.model.Feature;
import com.interview.nuvalence.model.Response;

@FunctionalInterface
public interface ShapeService {
    Response validateShapes(Feature analyzeType, JsonNode shape1, JsonNode shape2);
}
