package com.interview.nuvalence.api.service;

import com.interview.nuvalence.api.exception.InvalidShapeException;
import com.interview.nuvalence.model.Point;

import java.util.List;

@FunctionalInterface
public interface BoundaryExtractor<T> {
    List<Point> extract(T shape) throws InvalidShapeException;
}
