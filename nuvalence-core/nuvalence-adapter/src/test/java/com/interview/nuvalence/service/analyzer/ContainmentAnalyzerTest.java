package com.interview.nuvalence.service.analyzer;

import com.interview.nuvalence.api.exception.InvalidShapeException;
import com.interview.nuvalence.api.service.BoundaryExtractor;
import com.interview.nuvalence.api.service.FeatureAnalyzer;
import com.interview.nuvalence.model.Feature;
import com.interview.nuvalence.model.Point;
import com.interview.nuvalence.model.Rectangle;
import com.interview.nuvalence.model.Response;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import net.javacrumbs.jsonunit.core.Configuration;
import net.javacrumbs.jsonunit.core.Option;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Stream;

import static com.interview.nuvalence.model.CustomObjectMapper.OBJECT_MAPPER;
import static java.util.Collections.emptyList;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;

@MicronautTest
public class ContainmentAnalyzerTest {
    private FeatureAnalyzer<Rectangle> testSubject;

    @Inject
    private BoundaryExtractor<Rectangle> rectangleService;

    @BeforeEach
    void setUp() {
        testSubject = new ContainmentAnalyzer(rectangleService);
    }

    @ParameterizedTest
    @MethodSource("provideNoContainmentExpectedAndRectangle")
    void findContainment_givenTwoRectanglesWithNoContainment_thenReturnNoContainmentResponse(Rectangle first, Rectangle second) throws InvalidShapeException {
        Response result = testSubject.analyze(first, second);

        Assertions.assertEquals(Feature.CONTAINMENT, result.getFeature());
        Assertions.assertEquals("No Containment", result.getFeatureSubType());
        assertJsonEquals(OBJECT_MAPPER.valueToTree(emptyList()), OBJECT_MAPPER.valueToTree(result.getFeaturePoints()));
    }

    @ParameterizedTest
    @MethodSource("provideContainmentExpectedAndRectangle")
    void findContainment_givenTwoRectangle_thenReturnContainmentPoints(List<Point> expected, Rectangle first, Rectangle second) throws InvalidShapeException {
        Response result = testSubject.analyze(first, second);

        Assertions.assertEquals(Feature.CONTAINMENT, result.getFeature());
        Assertions.assertEquals("Containment", result.getFeatureSubType());
        assertJsonEquals(OBJECT_MAPPER.valueToTree(expected), OBJECT_MAPPER.valueToTree(result.getFeaturePoints()), Configuration.empty().withOptions(Option.IGNORING_ARRAY_ORDER));
    }

    @ParameterizedTest
    @MethodSource("provideNoContainmentButIntersectionExpectedAndRectangle")
    void findNoContainmentIntersection_givenTwoRectangle_whenNoContainment_thenReturnPoints(List<Point> expected, Rectangle first, Rectangle second) throws InvalidShapeException {
        Response result = testSubject.analyze(first, second);

        Assertions.assertEquals(Feature.CONTAINMENT, result.getFeature());
        Assertions.assertEquals("Intersection - No Containment", result.getFeatureSubType());
        assertJsonEquals(OBJECT_MAPPER.valueToTree(expected), OBJECT_MAPPER.valueToTree(result.getFeaturePoints()), Configuration.empty().withOptions(Option.IGNORING_ARRAY_ORDER));
    }

    private static Stream<Arguments> provideNoContainmentExpectedAndRectangle() {
        return Stream.of(
                Arguments.of(getRectangle(0, 0, 2, 2),
                        getRectangle(2, 3, 10, 10)),
                Arguments.of(getRectangle(0, 0, 5, 5),
                        getRectangle(0, 6, 10, 10))
        );
    }

    private static Stream<Arguments> provideContainmentExpectedAndRectangle() {
        return Stream.of(
                Arguments.of(expectedPoints(2, 3, 7, 9),
                        getRectangle(0, 0, 10, 10),
                        getRectangle(2, 3, 5, 6)),
                Arguments.of(expectedPoints(12, 12, 17, 17),
                        getRectangle(12, 12, 5, 5),
                        getRectangle(10, 10, 10, 10))
        );
    }

    private static Stream<Arguments> provideNoContainmentButIntersectionExpectedAndRectangle() {
        return Stream.of(
                Arguments.of(expectedPoints(8, 6, 10, 8),
                        getRectangle(0, 0, 10, 9),
                        getRectangle(8, 6, 5, 2)),
                Arguments.of(expectedPoints(8, 6, 10, 8),
                        getRectangle(8, 6, 5, 2),
                        getRectangle(0, 0, 10, 10))
        );
    }

    private static Rectangle getRectangle(int pointX, int pointY, int width, int height) {
        return new Rectangle()
                .withInitialPoint(new Point().withPointX(pointX).withPointY(pointY))
                .withWidth(width).withHeight(height);
    }

    private static List<Point> expectedPoints(int pointX, int pointY, int pointX1, int pointY1) {
        return List.of(new Point(pointX, pointY),
                new Point(pointX, pointY1),
                new Point(pointX1, pointY),
                new Point(pointX1, pointY1));
    }

}
