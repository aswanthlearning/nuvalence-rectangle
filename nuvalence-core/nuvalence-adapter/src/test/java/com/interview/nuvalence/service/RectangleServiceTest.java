package com.interview.nuvalence.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.interview.nuvalence.model.Feature;
import com.interview.nuvalence.model.Point;
import com.interview.nuvalence.model.Rectangle;
import com.interview.nuvalence.model.Response;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import net.javacrumbs.jsonunit.core.Configuration;
import net.javacrumbs.jsonunit.core.Option;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Stream;

import static com.interview.nuvalence.model.CustomObjectMapper.OBJECT_MAPPER;
import static java.util.Collections.emptyList;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.junit.jupiter.api.Assertions.*;

@MicronautTest
public class RectangleServiceTest {

    private RectangleService testSubject;

    @Inject
    private AnalyzerFactory factory;

    @BeforeEach
    void setUp() {
        testSubject = new RectangleService(factory);
    }

    @Test
    void validateNullShape_givenFeatureType_thenThrowRunTimeException() {
        Rectangle first = new Rectangle().withInitialPoint(new Point(0, 0)).withWidth(5).withHeight(5);
        JsonNode node = OBJECT_MAPPER.convertValue(first, JsonNode.class);
        RuntimeException ex = assertThrows(RuntimeException.class,
                () -> testSubject.validateShapes(Feature.INTERSECTION, node, null));

        assertEquals("Shape cannot be null", ex.getMessage());
        assertNotNull(ex.getCause());
    }

    @ParameterizedTest
    @MethodSource("provideIntersectionExpectedAndInput")
    void validateShape_givenFeatureType_ReturnResponse(ObjectNode node1, ObjectNode node2, Response expected) {
        Response response = testSubject.validateShapes(Feature.INTERSECTION, node1, node2);

        assertAll("Checking the intersection responses",
                () -> assertNotNull(response),
                () -> assertEquals(expected.getFeature(), response.getFeature()),
                () -> assertEquals(expected.getFeatureSubType(), response.getFeatureSubType()),
                () -> assertJsonEquals(OBJECT_MAPPER.valueToTree(expected.getFeaturePoints()),
                        OBJECT_MAPPER.valueToTree(response.getFeaturePoints()),
                        Configuration.empty().withOptions(Option.IGNORING_ARRAY_ORDER)));
    }

    @ParameterizedTest
    @MethodSource("provideContainmentExpectedAndInput")
    void validateShapeForContainment_givenFeatureType_ReturnResponse(ObjectNode node1, ObjectNode node2, Response expected) {
        Response response = testSubject.validateShapes(Feature.CONTAINMENT, node1, node2);

        assertAll("Checking the containment responses",
                () -> assertNotNull(response),
                () -> assertEquals(expected.getFeature(), response.getFeature()),
                () -> assertEquals(expected.getFeatureSubType(), response.getFeatureSubType()),
                () -> assertJsonEquals(OBJECT_MAPPER.valueToTree(expected.getFeaturePoints()),
                        OBJECT_MAPPER.valueToTree(response.getFeaturePoints()),
                        Configuration.empty().withOptions(Option.IGNORING_ARRAY_ORDER)));
    }

    @ParameterizedTest
    @MethodSource("provideAdjacentExpectedAndInput")
    void validateShapeForAdjacent_givenFeatureType_ReturnResponse(ObjectNode node1, ObjectNode node2, Response expected) {
        Response response = testSubject.validateShapes(Feature.ADJACENT, node1, node2);

        assertAll("Checking the Adjacent responses",
                () -> assertNotNull(response),
                () -> assertEquals(expected.getFeature(), response.getFeature()),
                () -> assertEquals(expected.getFeatureSubType(), response.getFeatureSubType()),
                () -> assertJsonEquals(OBJECT_MAPPER.valueToTree(expected.getFeaturePoints()),
                        OBJECT_MAPPER.valueToTree(response.getFeaturePoints()),
                        Configuration.empty().withOptions(Option.IGNORING_ARRAY_ORDER)));
    }

    private static Stream<Arguments> provideIntersectionExpectedAndInput() {
        return Stream.of(
                Arguments.of(loadRectangleNode(0, 0, 10, 10),
                        loadRectangleNode(5, -2, 2, 6),
                        new Response().withFeature(Feature.INTERSECTION)
                                .withFeatureSubType("Intersection")
                                .withFeaturePoints(expectedPoints(5, 0, 7, 4))),
                Arguments.of(loadRectangleNode(0, 0, 10, 10),
                        loadRectangleNode(11, 0, 2, 6),
                        new Response().withFeature(Feature.INTERSECTION)
                                .withFeatureSubType("No Intersection")
                                .withFeaturePoints(emptyList())));
    }

    private static Stream<Arguments> provideContainmentExpectedAndInput() {
        return Stream.of(
                Arguments.of(loadRectangleNode(0, 0, 10, 10),
                        loadRectangleNode(-5, -5, 2, 2),
                        new Response().withFeature(Feature.CONTAINMENT)
                                .withFeatureSubType("No Containment")
                                .withFeaturePoints(emptyList()),
                        Arguments.of(loadRectangleNode(0, 0, 10, 10),
                                loadRectangleNode(2, 2, 2, 6),
                                new Response().withFeature(Feature.CONTAINMENT)
                                        .withFeatureSubType("Containment")
                                        .withFeaturePoints(expectedPoints(2, 2, 4, 8)))),
                Arguments.of(loadRectangleNode(0, 0, 10, 10),
                        loadRectangleNode(2, 2, 2, 10),
                        new Response().withFeature(Feature.CONTAINMENT)
                                .withFeatureSubType("Intersection - No Containment")
                                .withFeaturePoints(expectedPoints(2, 2, 4, 10))));
    }

    private static Stream<Arguments> provideAdjacentExpectedAndInput() {
        return Stream.of(
                Arguments.of(loadRectangleNode(0, 0, 10, 10),
                        loadRectangleNode(10, 0, 5, 10),
                        new Response().withFeature(Feature.ADJACENT)
                                .withFeatureSubType("Adjacent (Proper)")
                                .withFeaturePoints(expectedAdjacentPoints(10, 0, 10, 10))),
                Arguments.of(loadRectangleNode(0, 0, 10, 10),
                        loadRectangleNode(10, 3, 5, 10),
                        new Response().withFeature(Feature.ADJACENT)
                                .withFeatureSubType("Adjacent (Partial)")
                                .withFeaturePoints(expectedAdjacentPoints(10, 3, 10, 13))));
    }

    private static List<Point> expectedPoints(int pointX, int pointY, int pointX1, int pointY1) {
        return List.of(new Point(pointX, pointY),
                new Point(pointX1, pointY1),
                new Point(pointX, pointY1),
                new Point(pointX1, pointY));
    }

    private static List<Point> expectedAdjacentPoints(int pointX, int pointY, int pointX1, int pointY1) {
        return List.of(new Point(pointX, pointY),
                new Point(pointX1, pointY1));
    }

    private static ObjectNode loadRectangleNode(int x, int y, int width, int height) {
        ObjectNode node = OBJECT_MAPPER.createObjectNode()
                .put("width", width)
                .put("height", height);
        node.putObject("initialPoint")
                .put("pointX", x)
                .put("pointY", y);
        return node;
    }
}
