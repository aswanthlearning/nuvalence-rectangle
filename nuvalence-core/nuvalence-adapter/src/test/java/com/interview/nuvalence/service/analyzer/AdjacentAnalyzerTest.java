package com.interview.nuvalence.service.analyzer;

import com.interview.nuvalence.api.exception.InvalidShapeException;
import com.interview.nuvalence.api.service.BoundaryExtractor;
import com.interview.nuvalence.api.service.FeatureAnalyzer;
import com.interview.nuvalence.model.Feature;
import com.interview.nuvalence.model.Point;
import com.interview.nuvalence.model.Rectangle;
import com.interview.nuvalence.model.Response;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Stream;

import static com.interview.nuvalence.model.CustomObjectMapper.OBJECT_MAPPER;
import static java.util.Collections.emptyList;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;

@MicronautTest
public class AdjacentAnalyzerTest {
    private FeatureAnalyzer<Rectangle> testSubject;

    @Inject
    private BoundaryExtractor<Rectangle> rectangleService;

    @BeforeEach
    void setUp() {
        testSubject = new AdjacentAnalyzer(rectangleService);
    }

    @ParameterizedTest
    @MethodSource("provideProperAdjacentExpectedAndRectangle")
    void findAdjacent_givenTwoRectangle_thenReturnAdjacentPoints(List<Point> expected, Rectangle first, Rectangle second) throws InvalidShapeException {
        Response result = testSubject.analyze(first, second);

        Assertions.assertEquals(Feature.ADJACENT, result.getFeature());
        Assertions.assertEquals("Adjacent (Proper)", result.getFeatureSubType());
        assertJsonEquals(OBJECT_MAPPER.valueToTree(expected), OBJECT_MAPPER.valueToTree(result.getFeaturePoints()));
    }

    @ParameterizedTest
    @MethodSource("providePartialAdjacentExpectedAndRectangle")
    void findPartialAdjacent_givenTwoRectangle_thenReturnAdjacentPoints(List<Point> expected, Rectangle first, Rectangle second) throws InvalidShapeException {
        Response result = testSubject.analyze(first, second);

        Assertions.assertEquals(Feature.ADJACENT, result.getFeature());
        Assertions.assertEquals("Adjacent (Partial)", result.getFeatureSubType());
        assertJsonEquals(OBJECT_MAPPER.valueToTree(expected), OBJECT_MAPPER.valueToTree(result.getFeaturePoints()));
    }

    @ParameterizedTest
    @MethodSource("provideSublineAdjacentExpectedAndRectangle")
    void findSublineAdjacent_givenTwoRectangle_thenReturnAdjacentPoints(List<Point> expected, Rectangle first, Rectangle second) throws InvalidShapeException {
        Response result = testSubject.analyze(first, second);

        Assertions.assertEquals(Feature.ADJACENT, result.getFeature());
        Assertions.assertEquals("Adjacent (Sub-line)", result.getFeatureSubType());
        assertJsonEquals(OBJECT_MAPPER.valueToTree(expected), OBJECT_MAPPER.valueToTree(result.getFeaturePoints()));
    }

    @ParameterizedTest
    @MethodSource("provideNoAdjacentExpectedAndRectangle")
    void findNoAdjacent_givenTwoRectangle_thenReturnAdjacentPoints(Rectangle first, Rectangle second) throws InvalidShapeException {
        Response result = testSubject.analyze(first, second);

        Assertions.assertEquals(Feature.ADJACENT, result.getFeature());
        Assertions.assertEquals("Not Adjacent", result.getFeatureSubType());
        assertJsonEquals(OBJECT_MAPPER.valueToTree(emptyList()), OBJECT_MAPPER.valueToTree(result.getFeaturePoints()));
    }

    private static Stream<Arguments> provideNoAdjacentExpectedAndRectangle() {
        return Stream.of(
                Arguments.of(getRectangle(0, 0, 10, 10),
                        getRectangle(11, 0, 5, 10)),
                Arguments.of(getRectangle(0, 0, 5, 5),
                        getRectangle(0, 11, 5, 10))
        );
    }

    private static Stream<Arguments> provideProperAdjacentExpectedAndRectangle() {
        return Stream.of(
                Arguments.of(expectedPoints(10, 0, 10, 10),
                        getRectangle(0, 0, 10, 10),
                        getRectangle(10, 0, 5, 10)),
                Arguments.of(expectedPoints(0, 5, 5, 5),
                        getRectangle(0, 0, 5, 5),
                        getRectangle(0, 5, 5, 10))
        );
    }

    private static Stream<Arguments> providePartialAdjacentExpectedAndRectangle() {
        return Stream.of(
                Arguments.of(expectedPoints(10, 3, 10, 13),
                        getRectangle(0, 0, 10, 10),
                        getRectangle(10, 3, 5, 10)),
                Arguments.of(expectedPoints(2, 5, 14, 5),
                        getRectangle(5, 5, 5, 5),
                        getRectangle(2, 1, 12, 4)),
                Arguments.of(expectedPoints(0, 1, 0, 5),
                        getRectangle(-5, -2, 5, 4),
                        getRectangle(0, 1, 2, 4))
        );
    }

    private static Stream<Arguments> provideSublineAdjacentExpectedAndRectangle() {
        return Stream.of(
                Arguments.of(expectedPoints(10, 3, 10, 6),
                        getRectangle(0, 0, 10, 10),
                        getRectangle(10, 3, 5, 3)),
                Arguments.of(expectedPoints(5, 0, 7, 0),
                        getRectangle(0, 0, 10, 10),
                        getRectangle(5, -5, 2, 5))
        );
    }

    private static Rectangle getRectangle(int pointX, int pointY, int width, int height) {
        return new Rectangle()
                .withInitialPoint(new Point().withPointX(pointX).withPointY(pointY))
                .withWidth(width).withHeight(height);
    }

    private static List<Point> expectedPoints(int pointX, int pointY, int pointX1, int pointY1) {
        return List.of(new Point(pointX, pointY),
                new Point(pointX1, pointY1));
    }

}
