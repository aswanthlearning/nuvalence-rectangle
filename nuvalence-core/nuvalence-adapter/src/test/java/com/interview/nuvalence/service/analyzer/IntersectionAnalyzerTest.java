package com.interview.nuvalence.service.analyzer;

import com.interview.nuvalence.api.exception.InvalidShapeException;
import com.interview.nuvalence.api.service.BoundaryExtractor;
import com.interview.nuvalence.api.service.FeatureAnalyzer;
import com.interview.nuvalence.model.Feature;
import com.interview.nuvalence.model.Point;
import com.interview.nuvalence.model.Rectangle;
import com.interview.nuvalence.model.Response;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Stream;

import static com.interview.nuvalence.model.CustomObjectMapper.OBJECT_MAPPER;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;

@MicronautTest
public class IntersectionAnalyzerTest {
    private FeatureAnalyzer<Rectangle> testSubject;

    @Inject
    private BoundaryExtractor<Rectangle> rectangleService;

    @BeforeEach
    void setUp() {
        testSubject = new IntersectionAnalyzer(rectangleService);
    }

    @Test
    void findIntersection_givenTwoRectanglesWithNoIntersection_thenReturnNoIntersectionResponse() throws InvalidShapeException {
        Rectangle first = getRectangle(0, 0, 2, 2);
        Rectangle second = getRectangle(2, 3, 10, 10);

        Response result = testSubject.analyze(first, second);

        Assertions.assertEquals(Feature.INTERSECTION, result.getFeature());
        Assertions.assertEquals("No Intersection", result.getFeatureSubType());
        assertJsonEquals(OBJECT_MAPPER.valueToTree(List.of()), OBJECT_MAPPER.valueToTree(result.getFeaturePoints()));
    }

    @ParameterizedTest
    @MethodSource("provideExpectedAndRectangle")
    void findIntersection_givenTwoRectangle_thenReturnIntersectionPoints(List<Point> expected, Rectangle first, Rectangle second) throws InvalidShapeException {
        Response result = testSubject.analyze(first, second);

        Assertions.assertEquals(Feature.INTERSECTION, result.getFeature());
        Assertions.assertEquals("Intersection", result.getFeatureSubType());
        assertJsonEquals(OBJECT_MAPPER.valueToTree(expected), OBJECT_MAPPER.valueToTree(result.getFeaturePoints()));
    }

    private static Stream<Arguments> provideExpectedAndRectangle() {
        return Stream.of(
                Arguments.of(expectedPoints(2, 3, 7, 8),
                        getRectangle(0, 0, 10, 8),
                        getRectangle(2, 3, 5, 6)),
                Arguments.of(expectedPoints(1, 1, 3, 3),
                        getRectangle(0, 0, 3, 3),
                        getRectangle(1, 1, 2, 2)),
                Arguments.of(expectedPoints(3, 2, 8, 5),
                        getRectangle(0, 2, 10, 8),
                        getRectangle(3, 0, 5, 5)),
                Arguments.of(expectedPoints(8, 6, 10, 8),
                        getRectangle(0, 0, 10, 9),
                        getRectangle(8, 6, 5, 2))
        );
    }

    private static Rectangle getRectangle(int pointX, int pointY, int width, int height) {
        return new Rectangle()
                .withInitialPoint(new Point().withPointX(pointX).withPointY(pointY))
                .withWidth(width).withHeight(height);
    }

    private static List<Point> expectedPoints(int pointX, int pointY, int pointX1, int pointY1) {
        return List.of(new Point(pointX, pointY),
                new Point(pointX1, pointY1),
                new Point(pointX, pointY1),
                new Point(pointX1, pointY));
    }

}
