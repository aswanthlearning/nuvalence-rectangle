package com.interview.nuvalence.service.extractor;

import com.interview.nuvalence.api.exception.InvalidShapeException;
import com.interview.nuvalence.api.service.BoundaryExtractor;
import com.interview.nuvalence.model.Point;
import com.interview.nuvalence.model.Rectangle;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

import static com.interview.nuvalence.model.CustomObjectMapper.OBJECT_MAPPER;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

@MicronautTest
public class RectangleExtractorTest {
    private BoundaryExtractor<Rectangle> testSubject;

    @BeforeEach
    void setUp() {
        testSubject = new RectangleExtractor();
    }

    @Test
    void givenRectangle_whenNegativeWidthArePasses_thenThrowInvalidShapeException() {
        InvalidShapeException ex = Assertions.assertThrows(InvalidShapeException.class, () ->
                testSubject.extract(getRectangle(-10, -20, -10, -10)));

        assertEquals("Height -10 and Width -10 cannot be negative", ex.getMessage());
    }

    @ParameterizedTest
    @MethodSource("provideExpectedAndRectangle")
    void givenRectangle_whenExtractPoint_thenReturnBoundaryPoints(List<Point> expected, Rectangle input) throws InvalidShapeException {
        var actual = testSubject.extract(input);
        assertJsonEquals(OBJECT_MAPPER.valueToTree(expected), OBJECT_MAPPER.valueToTree(actual));
    }

    private static Stream<Arguments> provideExpectedAndRectangle() {
        return Stream.of(
                Arguments.of(expectedPoints(0, 0, 10, 5),
                        getRectangle(0, 0, 10, 5)),
                Arguments.of(expectedPoints(-10, -20, 0, -10),
                        getRectangle(-10, -20, 10, 10))
        );
    }

    private static Rectangle getRectangle(int pointX, int pointY, int width, int height) {
        return new Rectangle()
                .withInitialPoint(new Point().withPointX(pointX).withPointY(pointY))
                .withWidth(width).withHeight(height);
    }

    private static List<Point> expectedPoints(int pointX1, int pointY1, int pointX2, int pointY2) {
        return List.of(new Point().withPointX(pointX1).withPointY(pointY1),
                new Point().withPointX(pointX2).withPointY(pointY1),
                new Point().withPointX(pointX1).withPointY(pointY2),
                new Point().withPointX(pointX2).withPointY(pointY2));
    }
}
