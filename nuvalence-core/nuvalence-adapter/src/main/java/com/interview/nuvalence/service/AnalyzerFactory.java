package com.interview.nuvalence.service;

import com.interview.nuvalence.api.service.BoundaryExtractor;
import com.interview.nuvalence.api.service.FeatureAnalyzer;
import com.interview.nuvalence.model.Feature;
import com.interview.nuvalence.model.Rectangle;
import com.interview.nuvalence.service.analyzer.AdjacentAnalyzer;
import com.interview.nuvalence.service.analyzer.ContainmentAnalyzer;
import com.interview.nuvalence.service.analyzer.IntersectionAnalyzer;

import javax.inject.Singleton;
import java.util.HashMap;
import java.util.Map;

@Singleton
public class AnalyzerFactory {
    private final BoundaryExtractor<Rectangle> rectangleService;
    private Map<Feature, FeatureAnalyzer<Rectangle>> analyzers;

    public AnalyzerFactory(BoundaryExtractor<Rectangle> rectangleService) {
        this.rectangleService = rectangleService;
        analyzers = new HashMap<>();
        createAnalyzers();
    }

    public FeatureAnalyzer<Rectangle> findAnalyzer(Feature feature) {
        return analyzers.get(feature);
    }

    private void createAnalyzers() {
        analyzers.put(Feature.INTERSECTION, new IntersectionAnalyzer(rectangleService));
        analyzers.put(Feature.CONTAINMENT, new ContainmentAnalyzer(rectangleService));
        analyzers.put(Feature.ADJACENT, new AdjacentAnalyzer(rectangleService));
    }
}
