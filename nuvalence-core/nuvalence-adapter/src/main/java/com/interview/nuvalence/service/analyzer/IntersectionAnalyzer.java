package com.interview.nuvalence.service.analyzer;

import com.interview.nuvalence.api.exception.InvalidShapeException;
import com.interview.nuvalence.api.service.BoundaryExtractor;
import com.interview.nuvalence.api.service.FeatureAnalyzer;
import com.interview.nuvalence.model.Feature;
import com.interview.nuvalence.model.Point;
import com.interview.nuvalence.model.Rectangle;
import com.interview.nuvalence.model.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Singleton
public class IntersectionAnalyzer implements FeatureAnalyzer<Rectangle> {
    private static final Logger LOGGER = LoggerFactory.getLogger(IntersectionAnalyzer.class);

    private final BoundaryExtractor<Rectangle> rectangleService;

    public IntersectionAnalyzer(BoundaryExtractor<Rectangle> rectangleService) {
        this.rectangleService = rectangleService;
    }

    @Override
    public Response analyze(Rectangle shape1, Rectangle shape2) throws InvalidShapeException {
        var firstRectangleBoundaries = rectangleService.extract(shape1);
        var secondRectangleBoundaries = rectangleService.extract(shape2);
        var intersectionPoints = new ArrayList<Point>();

        long bottomLeftX = getMax(firstRectangleBoundaries.get(0).getPointX(), secondRectangleBoundaries.get(0).getPointX());
        long topRightX = getXMin(firstRectangleBoundaries, secondRectangleBoundaries);
        long bottomLeftY = getMax(firstRectangleBoundaries.get(0).getPointY(), secondRectangleBoundaries.get(0).getPointY());
        long topRightY = getYmin(firstRectangleBoundaries, secondRectangleBoundaries);

        LOGGER.info("The rectangle points are bottomLeftX -> {}, bottomLeftY -> {}, topRightX -> {}, topRightY -> {}",
                bottomLeftX, bottomLeftY, topRightX, topRightY);

        if (hasNoIntersection(bottomLeftX, topRightX, bottomLeftY, topRightY)) {
            return new Response().withFeature(Feature.INTERSECTION).withFeatureSubType("No Intersection").withFeaturePoints(Collections.emptyList());
        }

        return validIntersectionResponse(intersectionPoints, bottomLeftX, topRightX, bottomLeftY, topRightY);
    }

    private Response validIntersectionResponse(ArrayList<Point> intersectionPoints, long bottomLeftX, long topRightX, long bottomLeftY, long topRightY) {
        intersectionPoints.add(new Point(bottomLeftX, bottomLeftY));
        intersectionPoints.add(new Point(topRightX, topRightY));
        intersectionPoints.add(new Point(bottomLeftX, topRightY));
        intersectionPoints.add(new Point(topRightX, bottomLeftY));
        return new Response().withFeature(Feature.INTERSECTION).withFeatureSubType("Intersection").withFeaturePoints(intersectionPoints);
    }

    private boolean hasNoIntersection(long bottomLeftX, long topRightX, long bottomLeftY, long topRightY) {
        return bottomLeftX > topRightX || bottomLeftY > topRightY;
    }

    private long getYmin(List<Point> firstRectangleBoundaries, List<Point> secondRectangleBoundaries) {
        return Math.min(firstRectangleBoundaries.get(firstRectangleBoundaries.size() - 1).getPointY(),
                secondRectangleBoundaries.get(secondRectangleBoundaries.size() - 1).getPointY());
    }

    private long getXMin(List<Point> firstRectangleBoundaries, List<Point> secondRectangleBoundaries) {
        return Math.min(firstRectangleBoundaries.get(firstRectangleBoundaries.size() - 1).getPointX(),
                secondRectangleBoundaries.get(secondRectangleBoundaries.size() - 1).getPointX());
    }

    private long getMax(long pointX, long pointX2) {
        return Math.max(pointX, pointX2);
    }
}
