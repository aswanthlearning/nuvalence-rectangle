package com.interview.nuvalence.service.analyzer;

import com.interview.nuvalence.api.exception.InvalidShapeException;
import com.interview.nuvalence.api.service.BoundaryExtractor;
import com.interview.nuvalence.api.service.FeatureAnalyzer;
import com.interview.nuvalence.model.Feature;
import com.interview.nuvalence.model.Point;
import com.interview.nuvalence.model.Rectangle;
import com.interview.nuvalence.model.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import java.util.Collections;
import java.util.List;

@Singleton
public class ContainmentAnalyzer implements FeatureAnalyzer<Rectangle> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ContainmentAnalyzer.class);

    private final BoundaryExtractor<Rectangle> rectangleService;
    private final IntersectionAnalyzer intersectionAnalyzer;

    public ContainmentAnalyzer(BoundaryExtractor<Rectangle> rectangleService) {
        this.rectangleService = rectangleService;
        this.intersectionAnalyzer = new IntersectionAnalyzer(rectangleService);
    }

    @Override
    public Response analyze(Rectangle shape1, Rectangle shape2) throws InvalidShapeException {
        var firstRectangleBoundaries = rectangleService.extract(shape1);
        var secondRectangleBoundaries = rectangleService.extract(shape2);

        Point firstRectangleBottomLeft = firstRectangleBoundaries.get(0);
        Point firstRectangleTopRight = firstRectangleBoundaries.get(firstRectangleBoundaries.size() - 1);
        Point secondRectangleBottomLeft = secondRectangleBoundaries.get(0);
        Point secondRectangleTopRight = secondRectangleBoundaries.get(secondRectangleBoundaries.size() - 1);

        boolean isContainmentBottomLeft = isContainmentBottomLeft(secondRectangleBoundaries, firstRectangleBottomLeft);
        boolean isContainmentTopRight = isContainmentTopRight(secondRectangleBoundaries, firstRectangleTopRight);

        if (isContainmentBottomLeft && isContainmentTopRight) {
            return new Response().withFeature(Feature.CONTAINMENT).withFeatureSubType("Containment").withFeaturePoints(secondRectangleBoundaries);
        }
        if (isContainmentBottomLeft || isContainmentTopRight) {
            return getResponseFromIntersection(shape1, shape2);
        }

        isContainmentBottomLeft = isContainmentBottomLeft(firstRectangleBoundaries, secondRectangleBottomLeft);
        isContainmentTopRight = isContainmentTopRight(firstRectangleBoundaries, secondRectangleTopRight);

        if (isContainmentBottomLeft && isContainmentTopRight) {
            return new Response().withFeature(Feature.CONTAINMENT).withFeatureSubType("Containment").withFeaturePoints(firstRectangleBoundaries);
        }
        if (isContainmentBottomLeft || isContainmentTopRight) {
            return getResponseFromIntersection(shape2, shape1);
        }

        return new Response().withFeature(Feature.CONTAINMENT).withFeatureSubType("No Containment").withFeaturePoints(Collections.emptyList());
    }

    private Response getResponseFromIntersection(Rectangle shape1, Rectangle shape2) throws InvalidShapeException {
        Response response = intersectionAnalyzer.analyze(shape1, shape2);
        response.setFeature(Feature.CONTAINMENT);
        if (response.getFeatureSubType().equals("No Intersection")) {
            response.setFeatureSubType("No Containment");
        } else {
            response.setFeatureSubType("Intersection - No Containment");
        }
        return response;
    }

    private boolean isContainmentTopRight(List<Point> secondRectangleBoundaries, Point firstRectangleTopRight) {
        return secondRectangleBoundaries.stream()
                .allMatch(point1 -> point1.getPointX() < firstRectangleTopRight.getPointX() && point1.getPointY() < firstRectangleTopRight.getPointY());
    }

    private boolean isContainmentBottomLeft(List<Point> secondRectangleBoundaries, Point firstRectangleBottomLeft) {
        return secondRectangleBoundaries.stream()
                .allMatch(point1 -> point1.getPointX() > firstRectangleBottomLeft.getPointX() && point1.getPointY() > firstRectangleBottomLeft.getPointY());
    }
}
