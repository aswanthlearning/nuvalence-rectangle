package com.interview.nuvalence.service.analyzer;

import com.interview.nuvalence.api.exception.InvalidShapeException;
import com.interview.nuvalence.api.service.BoundaryExtractor;
import com.interview.nuvalence.api.service.FeatureAnalyzer;
import com.interview.nuvalence.model.Feature;
import com.interview.nuvalence.model.Point;
import com.interview.nuvalence.model.Rectangle;
import com.interview.nuvalence.model.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

@Singleton
public class AdjacentAnalyzer implements FeatureAnalyzer<Rectangle> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AdjacentAnalyzer.class);

    private final BoundaryExtractor<Rectangle> rectangleService;

    public AdjacentAnalyzer(BoundaryExtractor<Rectangle> rectangleService) {
        this.rectangleService = rectangleService;
    }

    @Override
    public Response analyze(Rectangle shape1, Rectangle shape2) throws InvalidShapeException {
        var firstRectangleBoundaries = rectangleService.extract(shape1);
        var secondRectangleBoundaries = rectangleService.extract(shape2);

        List<Point> adjacentPoints = isProperAdjacent(firstRectangleBoundaries, secondRectangleBoundaries);
        if (adjacentPoints.size() == 2) {
            return new Response().withFeature(Feature.ADJACENT).withFeatureSubType("Adjacent (Proper)").withFeaturePoints(adjacentPoints);
        }

        adjacentPoints.clear();
        adjacentPoints = isPartialOrSubline(firstRectangleBoundaries, secondRectangleBoundaries);
        List<Point> adjacentPointsFromSecondRectangle = isPartialOrSubline(secondRectangleBoundaries, firstRectangleBoundaries);

        if (adjacentPoints.isEmpty() || adjacentPointsFromSecondRectangle.isEmpty()) {
            return new Response().withFeature(Feature.ADJACENT).withFeatureSubType("Not Adjacent").withFeaturePoints(emptyList());
        }

        if (adjacentPoints.size() < 4 && adjacentPointsFromSecondRectangle.size() < 4) {
            if (isPartial(adjacentPoints, adjacentPointsFromSecondRectangle)) {
                LOGGER.info("The Adjacency are partial");
                return new Response().withFeature(Feature.ADJACENT).withFeatureSubType("Adjacent (Partial)").withFeaturePoints(adjacentPointsFromSecondRectangle);
            }

            if (isSubline(adjacentPoints, adjacentPointsFromSecondRectangle)) {
                LOGGER.info("The Adjacency are subline");
                return new Response().withFeature(Feature.ADJACENT).withFeatureSubType("Adjacent (Sub-line)").withFeaturePoints(adjacentPointsFromSecondRectangle);
            }
        }
        return new Response().withFeature(Feature.ADJACENT).withFeatureSubType("Not Adjacent").withFeaturePoints(emptyList());
    }

    private List<Point> isPartialOrSubline(List<Point> firstRectangleBoundaries, List<Point> secondRectangleBoundaries) {
        List<Point> adjacentPoints;
        adjacentPoints = firstRectangleBoundaries.stream()
                .filter(point -> secondRectangleBoundaries.stream()
                        .anyMatch(p -> p.getPointX() == point.getPointX()
                                || p.getPointY() == point.getPointY()))
                .collect(toList());
        return adjacentPoints;
    }

    private List<Point> isProperAdjacent(List<Point> firstRectangleBoundaries, List<Point> secondRectangleBoundaries) {
        return firstRectangleBoundaries.stream()
                .filter(point -> secondRectangleBoundaries.stream()
                        .anyMatch(p -> p.getPointX() == point.getPointX()
                                && p.getPointY() == point.getPointY()))
                .collect(toList());
    }

    private boolean isSubline(List<Point> adjacentPointsSum, List<Point> valuesSum) {
        AtomicBoolean isSubline = new AtomicBoolean(true);
        adjacentPointsSum.forEach(point -> {
            valuesSum.forEach(pt -> {
                if (point.getPointX() == pt.getPointX()) {
                    isSubline.set(pt.getPointY() <= point.getPointY());
                } else if (point.getPointY() == pt.getPointY()) {
                    isSubline.set(pt.getPointX() <= point.getPointX());
                }
            });
        });
        return isSubline.get();
    }

    private boolean isPartial(List<Point> adjacentPointsSum, List<Point> valuesSum) {
        AtomicBoolean isPartial = new AtomicBoolean(false);
        adjacentPointsSum.forEach(point -> {
            valuesSum.forEach(pt -> {
                if (point.getPointX() == pt.getPointX()) {
                    isPartial.set(pt.getPointY() > point.getPointY());
                } else if (point.getPointY() == pt.getPointY()) {
                    isPartial.set(pt.getPointX() > point.getPointX());
                }
            });
        });
        return isPartial.get();
    }

}
