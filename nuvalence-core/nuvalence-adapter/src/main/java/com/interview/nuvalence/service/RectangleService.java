package com.interview.nuvalence.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.interview.nuvalence.api.exception.InvalidShapeException;
import com.interview.nuvalence.api.service.ShapeService;
import com.interview.nuvalence.model.Feature;
import com.interview.nuvalence.model.Rectangle;
import com.interview.nuvalence.model.Response;

import javax.inject.Singleton;

import static com.interview.nuvalence.model.CustomObjectMapper.OBJECT_MAPPER;

@Singleton
public class RectangleService implements ShapeService {
    private final AnalyzerFactory factory;

    public RectangleService(AnalyzerFactory factory) {
        this.factory = factory;
    }

    @Override
    public Response validateShapes(Feature analyzeType, JsonNode shape1, JsonNode shape2) {
        try {
            Rectangle firstShape = OBJECT_MAPPER.convertValue(shape1, Rectangle.class);
            Rectangle secondShape = OBJECT_MAPPER.convertValue(shape2, Rectangle.class);

            return factory.findAnalyzer(analyzeType).analyze(firstShape, secondShape);
        } catch (InvalidShapeException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
