package com.interview.nuvalence.service.extractor;

import com.interview.nuvalence.api.exception.InvalidShapeException;
import com.interview.nuvalence.api.service.BoundaryExtractor;
import com.interview.nuvalence.model.Point;
import com.interview.nuvalence.model.Rectangle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.util.Objects.isNull;

@Singleton
public class RectangleExtractor implements BoundaryExtractor<Rectangle> {
    private static final Logger LOGGER = LoggerFactory.getLogger(RectangleExtractor.class);

    @Override
    public List<Point> extract(Rectangle shape) throws InvalidShapeException {
        if (isNull(shape)) {
            throw new InvalidShapeException("Shape cannot be null");
        }
        LOGGER.info("The rectangle points are Points -> {}, width -> {}, height -> {}",
                shape.getInitialPoint(), shape.getWidth(), shape.getHeight());

        if (isNull(shape.getInitialPoint()) || shape.getHeight() < 0 || shape.getWidth() < 0) {
            throw new InvalidShapeException(String.format("Height %s and Width %s cannot be negative", shape.getHeight(), shape.getWidth()));
        }
        var validPoints = new ArrayList<Point>();
        Point point = shape.getInitialPoint();
        validPoints.add(point);
        validPoints.add(makePoint(point.getPointX() + shape.getWidth(), point.getPointY()));
        validPoints.add(makePoint(point.getPointX(), point.getPointY() + shape.getHeight()));
        validPoints.add(makePoint(point.getPointX() + shape.getWidth(), point.getPointY() + shape.getHeight()));
        validPoints.forEach(p -> LOGGER.info("The rectangle points are X -> {} and Y -> {}",
                p.getPointX(), p.getPointY()));

        return Collections.unmodifiableList(validPoints);
    }

    private Point makePoint(long pointX, long pointY) {
        return new Point().withPointX(pointX).withPointY(pointY);
    }
}
