package com.interview.nuvalence.model;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;

public class CustomObjectMapper {
    public final static ObjectMapper OBJECT_MAPPER = configureMapper(new ObjectMapper());

    private static ObjectMapper configureMapper(ObjectMapper objectMapper) {
        return objectMapper
                .registerModule(new Jdk8Module().configureAbsentsAsNulls(true))
                .enable(SerializationFeature.INDENT_OUTPUT)
                .enable(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT)
                .enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
    }
}
