FROM openjdk:12.0.2
EXPOSE 8080
COPY nuvalence-app/target/nuvalence-app-*.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]