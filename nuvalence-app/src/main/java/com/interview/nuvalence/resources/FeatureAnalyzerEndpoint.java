package com.interview.nuvalence.resources;

import com.interview.nuvalence.api.rest.FeatureAnalyzerResource;
import com.interview.nuvalence.api.service.ShapeService;
import com.interview.nuvalence.model.Feature;
import com.interview.nuvalence.model.Response;
import io.micronaut.http.annotation.Controller;

import java.util.List;

import static com.interview.nuvalence.model.CustomObjectMapper.OBJECT_MAPPER;

@Controller(value = "/api/v1")
public class FeatureAnalyzerEndpoint<T> implements FeatureAnalyzerResource<T> {
    private final ShapeService shapeService;

    public FeatureAnalyzerEndpoint(ShapeService shapeService) {
        this.shapeService = shapeService;
    }

    @Override
    public Response getResults(String featureType, List<T> shapes) {
        return shapeService.validateShapes(Feature.fromValue(featureType), OBJECT_MAPPER.valueToTree(shapes.get(0)), OBJECT_MAPPER.valueToTree(shapes.get(1)));
    }
}
