package com.interview.nuvalence.resources;

import com.interview.nuvalence.model.Point;
import com.interview.nuvalence.model.Rectangle;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.runtime.server.EmbeddedServer;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@MicronautTest
public class FeatureAnalyzerEndpointTest {

    @Inject
    EmbeddedServer server;

    @Inject
    @Client("/")
    HttpClient client;

    private static Rectangle getRectangle(int pointX, int pointY, int width, int height) {
        return new Rectangle()
                .withInitialPoint(new Point().withPointX(pointX).withPointY(pointY))
                .withWidth(width).withHeight(height);
    }

    @Test
    void testGetAnalyzerResultForIntersectionResponse() {
        Rectangle first = getRectangle(0, 0, 10, 10);
        Rectangle second = getRectangle(6, 8, 10, 10);

        HttpResponse<Object> response = client.toBlocking().exchange(HttpRequest.POST("/api/v1/shape/analyze/Intersection", List.of(first, second)));
        assertEquals(HttpStatus.OK, response.getStatus());
    }

    @Test
    void testGetAnalyzerForContainmentResultResponse() {
        Rectangle first = getRectangle(0, 0, 2, 2);
        Rectangle second = getRectangle(2, 3, 10, 10);

        HttpResponse<Object> response = client.toBlocking().exchange(HttpRequest.POST("/api/v1/shape/analyze/Containment", List.of(first, second)));
        assertEquals(HttpStatus.OK, response.getStatus());
    }

    @Test
    void testGetAnalyzerForAdjacentResultResponse() {
        Rectangle first = getRectangle(0, 0, 10, 10);
        Rectangle second = getRectangle(10, 3, 5, 10);

        HttpResponse<Object> response = client.toBlocking().exchange(HttpRequest.POST("/api/v1/shape/analyze/Adjacent", List.of(first, second)));
        assertEquals(HttpStatus.OK, response.getStatus());
    }
}
